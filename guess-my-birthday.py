import random

player_name = input("Enter name")

for guess_birthday in range (1, 6):
    year = (random.randint(1900, 2022))
    months = [
    "January",
    "Feburary",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
    ]
    month = (random.randint(0, 11))

    print("Attempt", guess_birthday, months[month], "/", year)

    answer = input("Is that a yes or a no?")

    if answer == ("yes"):
        print("I knew it!")
        exit()
    elif guess_birthday == 5:
        print("I have better things I could be doing.")
    else:
        print("Drat! Lemme try again!")
 